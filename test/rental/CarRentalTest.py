import unittest
from datetime import date, timedelta
from main.rental.Renter import Renter
from main.rental.Car import Car
from main.rental.Criteria import Criteria
from main.rental.CarRentalCompany import CarRentalCompany
from main.utils.DatePeriod import DatePeriod

class CarRentalTest(unittest.TestCase):
    
    # Create mock data
    CAR1 = Car("VW", "Golf", "XX11 1UR", "B2", 90)
    CAR2 = Car("VW", "Passat", "XX12 2UR", None, 110)
    CAR3 = Car("VW", "Polo", "XX13 3UR", "A1", 65)
    CAR4 = Car("VW", "Polo", "XX14 4UR", "A1", 70)

    RENTER1 = Renter("Hydrogen", "Joe", "HYDRO010190JX8NM", date(1990, 1, 1))
    RENTER2 = Renter("Calcium", "Sam", "CALCI010203SX8NM", date(2003, 2, 1))
    RENTER3 = Renter("Neon", "Maisy", "NEONN010398MX8NM", date(1998, 3, 1))
    RENTER4 = Renter("Carbon", "Greta", "CARBO010497GX8NM", date(1997, 4, 1))

    def test_list_cars_available_to_rent_gives_more_than_one_car(self):
        car_rental_company = CarRentalCompany()
        car_rental_company.add_car(self.CAR1)
        car_rental_company.add_car(self.CAR2)
        car_rental_company.add_car(self.CAR3)
        car_rental_company.add_car(self.CAR4)

        criteria = Criteria()
        cars_available = car_rental_company.matching_cars(criteria)

        self.assertGreater(len(cars_available), 1)

    def test_matching_cars_with_criteria(self):
        # Set up the CarRentalCompany with some cars
        car_rental_company = CarRentalCompany()
        car_rental_company.add_car(self.CAR1)
        car_rental_company.add_car(self.CAR2)
        car_rental_company.add_car(self.CAR3)
        car_rental_company.add_car(self.CAR4)

        # Define specific criteria for the car selection
        criteria = Criteria(make="VW", model="Polo", rental_group="A1")

        # Use the matching_cars method to find cars that meet the criteria
        matching_cars = car_rental_company.matching_cars(criteria)

        # Extract car objects from the matching results for direct comparison
        matching_car_objects = [match['car'] for match in matching_cars]

        # Assert that only the correct car(s) are returned based on the criteria
        self.assertEqual(len(matching_car_objects), 2)  # Expecting 2 matches (CAR3 and CAR4)
        self.assertNotIn(self.CAR1, matching_car_objects)
        self.assertNotIn(self.CAR2, matching_car_objects)
        self.assertIn(self.CAR3, matching_car_objects)
        self.assertIn(self.CAR4, matching_car_objects)
    
    def test_matching_cars_with_available_period(self):
        # Create car rental company
        rental_company = CarRentalCompany()
        
        # Add cars to the company's inventory
        rental_company.add_car(self.CAR1)
        rental_company.add_car(self.CAR2)
        rental_company.add_car(self.CAR3)

        # Create bookings
        period = DatePeriod(start=date(2024, 3, 1), end=date(2024, 3, 5))
        booking_result = rental_company.rent_car(self.RENTER1, self.CAR1, period)
        
        # Assert that the booking was successful
        self.assertTrue(booking_result)

        # Specify date range criteria
        criteria = Criteria(period=DatePeriod(date(2024, 3, 3), date(2024, 3, 7)))

        # Execute the matching cars method
        available_cars = rental_company.matching_cars(criteria)

        # Assert that only car2 and car3 are available during the specified date range
        self.assertEqual(len(available_cars), 2)
        available_car_objects = [car_info['car'] for car_info in available_cars]
        self.assertIn(self.CAR2, available_car_objects)
        self.assertIn(self.CAR3, available_car_objects)

    def test_booking_a_car(self):
        # Create a car rental company
        rental_company = CarRentalCompany()
        
        # Add cars to the company's inventory
        rental_company.add_car(self.CAR1)
        rental_company.add_car(self.CAR2)
        rental_company.add_car(self.CAR3)
        
        # Book a car
        period = DatePeriod(start=date(2024, 3, 1), end=date(2024, 3, 5))
        booking_result = rental_company.rent_car(self.RENTER1, self.CAR1, period)

        # Assert that the booking was successful
        self.assertTrue(booking_result)

        # # Attempt to book the same car for overlapping dates (should fail)
        overlapping_period = DatePeriod(start=date(2024, 3, 4), end=date(2024, 3, 10))
        overlapping_booking_result = rental_company.rent_car(self.RENTER2, self.CAR1, overlapping_period)

        # Assert that the overlapping booking failed
        self.assertFalse(overlapping_booking_result)
    
    def test_upcoming_rentals_for_the_week(self):
        # Create a car rental company
        rental_company = CarRentalCompany()

        # Add cars to the company's inventory
        rental_company.add_car(self.CAR1)
        rental_company.add_car(self.CAR2)
        rental_company.add_car(self.CAR3)

        # Book a car for the upcoming week
        start_date = date.today() + timedelta(days=1)  # Tomorrow
        end_date = date.today() + timedelta(days=7)    # 7 days from now
        period = DatePeriod(start=start_date, end=end_date)
        rental_company.rent_car(self.RENTER1, self.CAR1, period)

        # Book another car for the upcoming week
        start_date = date.today() + timedelta(days=4)  # 4 days from now
        end_date = date.today() + timedelta(days=10)    # 7 days from now
        period = DatePeriod(start=start_date, end=end_date)
        rental_company.rent_car(self.RENTER2, self.CAR2, period)

        # Book a car that is for the future and not the upcoming week
        start_date = date.today() + timedelta(days=10)  # 4 days from now
        end_date = date.today() + timedelta(days=17)    # 7 days from now
        period = DatePeriod(start=start_date, end=end_date)
        rental_company.rent_car(self.RENTER3, self.CAR3, period)

        # Retrieve upcoming rentals for the week
        upcoming_rentals = rental_company.get_new_rentals_for_next_week()

        # Assert that the rentals are ordered by date
        for i in range(len(upcoming_rentals) - 1):
            self.assertTrue(upcoming_rentals[i]['date_rented'] <= upcoming_rentals[i+1]['date_rented'])

        # Assert 2 cars needs to be cleaned this week
        expected_num_cars = 2
        actual_num_cars = len(upcoming_rentals)
        self.assertEqual(actual_num_cars, expected_num_cars)

        # Check the structure of the data returned
        for rental in upcoming_rentals:
            self.assertIn('date_rented', rental)
            self.assertIn('make', rental)
            self.assertIn('model', rental)
            self.assertIn('registration_number', rental)
            
    def test_servicing_a_car(self):
       
       # Create a car rental company
        rental_company = CarRentalCompany()

        # Add cars to the company's inventory
        rental_company.add_car(self.CAR1)
        rental_company.add_car(self.CAR2)
        rental_company.add_car(self.CAR3)

        # Book a car service
        start_date = date(2024, 4, 1)
        end_date = date(2024, 4, 5)
        period = DatePeriod(start=start_date, end=end_date)
        booking_result = rental_company.service_car(self.CAR1, period)
        
        # Assert that the booking was successful
        self.assertTrue(booking_result)
     
    def test_servicing_a_car_when_already_booked_same_group_available(self):
       
       # Create a car rental company
        rental_company = CarRentalCompany()

        # Add cars to the company's inventory
        rental_company.add_car(self.CAR1)
        rental_company.add_car(self.CAR2)
        rental_company.add_car(self.CAR3)
        rental_company.add_car(self.CAR4)

        # Rent car
        start_date = date(2024, 4, 1)
        end_date = date(2024, 4, 5)
        period = DatePeriod(start=start_date, end=end_date)
        booking_result = rental_company.rent_car(self.RENTER1, self.CAR3, period)
        
        # Assert that the booking was successful
        self.assertTrue(booking_result)

        # Service booked out car
        start_date = date(2024, 4, 3)
        end_date = date(2024, 4, 5)
        period = DatePeriod(start=start_date, end=end_date)
        booking_result = rental_company.service_car(self.CAR3, period)
        
        # Assert that the booking was not successful
        self.assertFalse(booking_result)

        # Transfer car
        start_date = date(2024, 4, 1)
        end_date = date(2024, 4, 5)
        period = DatePeriod(start=start_date, end=end_date)
        booking_result = rental_company.transfer_booking(self.RENTER1, self.CAR3, self.CAR4, period)
        # Assert that the booking was successful
        self.assertTrue(booking_result)

        # Service original car
        start_date = date(2024, 4, 3)
        end_date = date(2024, 4, 5)
        period = DatePeriod(start=start_date, end=end_date)
        booking_result = rental_company.service_car(self.CAR3, period)
        # Assert that the booking was successful
        self.assertTrue(booking_result)

        # all_bookings = rental_company.get_all_bookings()
        # for booking in all_bookings:
        #     print(booking)
    
    def test_transferring_car_to_different_group_is_not_allowed(self):
       
       # Create a car rental company
        rental_company = CarRentalCompany()

        # Add cars to the company's inventory
        rental_company.add_car(self.CAR1)
        rental_company.add_car(self.CAR2)
        rental_company.add_car(self.CAR3)
        rental_company.add_car(self.CAR4)

        # Rent car
        start_date = date(2024, 4, 1)
        end_date = date(2024, 4, 5)
        period = DatePeriod(start=start_date, end=end_date)
        booking_result = rental_company.rent_car(self.RENTER1, self.CAR1, period)
        # Assert that the booking was successful
        self.assertTrue(booking_result)

        # Transfer car
        start_date = date(2024, 4, 1)
        end_date = date(2024, 4, 5)
        period = DatePeriod(start=start_date, end=end_date)
        booking_result = rental_company.transfer_booking(self.RENTER1, self.CAR3, self.CAR4, period)
        # Assert that the booking was successful
        self.assertFalse(booking_result)

        # all_bookings = rental_company.get_all_bookings()
        # for booking in all_bookings:
        #     print(booking)

    def test_matching_cars_includes_blended_price_for_alpha(self):
        
        # Create a car rental company
        car_rental_company = CarRentalCompany()

        # Add cars to the company's inventory
        car_rental_company.add_car(self.CAR1)
        car_rental_company.add_car(self.CAR2)
        car_rental_company.add_car(self.CAR3)
        car_rental_company.add_car(self.CAR4)

        criteria = Criteria()  # Initialising without arguments for simplicity
        # Use the matching_cars method to find cars that meet the criteria
        matching_cars = car_rental_company.matching_cars(criteria)
        
        for car_info in matching_cars:
            if car_info["car"].rental_group == "A1":
                self.assertEqual(car_info["blended_price"], (self.CAR3.cost_per_day + self.CAR4.cost_per_day) / 2 ) # Blended price for Alpha (A1) 

            else:
                self.assertIsNone(car_info["blended_price"])  # Car not in Alpha (A1) should not have a blended price

    def test_rent_car_with_blended_price(self):
        # Create a car rental company
        car_rental_company = CarRentalCompany()

        # Add cars to the company's inventory
        car_rental_company.add_car(self.CAR1)
        car_rental_company.add_car(self.CAR2)
        car_rental_company.add_car(self.CAR3)
        car_rental_company.add_car(self.CAR4)

        # Rent car1 using the blended price
        start_date = date(2024, 4, 1)
        end_date = date(2024, 4, 5)
        period = DatePeriod(start=start_date, end=end_date)
        
        booking_result = car_rental_company.rent_car(self.RENTER1, self.CAR4, period, use_blended_price=True)

        # Assert that the booking was successful
        self.assertTrue(booking_result)

        blended_price = (self.CAR3.cost_per_day + self.CAR4.cost_per_day) / 2
        # Check if the booking was made with the blended price
        booking = car_rental_company.get_all_bookings()[-1]  # Get the most recent booking
        
        #print(f'\nBooking cost per day: {booking.cost_per_day} \nBlended price: {blended_price}')
        # Assert that the booking cost per day was made with the blended price
        self.assertEqual(booking.cost_per_day, blended_price)  # Blended price for Alpha (A1)

   
    
