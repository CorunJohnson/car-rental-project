from main.utils.DatePeriod import DatePeriod

class Criteria:
    def __init__(self, make=None, model=None, rental_group=None, max_cost_per_day=None, period=None):
        self.make = make
        self.model = model
        self.rental_group = rental_group
        self.max_cost_per_day = max_cost_per_day
        # Ensure that period is an instance of DatePeriod
        if period is not None:
            assert isinstance(period, DatePeriod), "period must be an instance of DatePeriod"

        self.period = period

