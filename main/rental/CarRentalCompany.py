import threading
from typing import List, Dict
from datetime import datetime, timedelta
from typing import List
from main.rental.Criteria import Criteria
from main.rental.Booking import Booking
from main.utils.DatePeriod import DatePeriod
from main.utils.DatePeriodUtil import DatePeriodUtil

class CarRentalCompany:
    def __init__(self):
        self.cars = []  # List to store cars
        self.bookings = []  # List to store bookings
        self.lock = threading.Lock()  # Initialise a lock for thread safety

    def add_car(self, car):
        with self.lock:  # Ensure thread-safe modifications to the car list
            if car not in self.cars:
                self.cars.append(car)
            else:
                raise ValueError("Car already exists in the inventory.")

    def matching_cars(self, criteria: Criteria):
        with self.lock:
            matching_cars_info = []

            for car in self.cars:
                if self._matches_criteria(car, criteria):
                    # If the car belongs to alpha group (A1), calculate and include the blended price
                    blended_price = None
                    if car.rental_group=="A1":
                        blended_price = self.get_blended_price(car.rental_group)
                    
                    car_info = {
                        "car": car,
                        "price": car.cost_per_day,  # Default to individual car's cost per day
                        "blended_price": blended_price
                    }

                    matching_cars_info.append(car_info)

            return matching_cars_info

    def _matches_criteria(self, car, criteria: Criteria):
        try:
            # Check if car matches the make criteria, if specified
            matches_make = not criteria.make or car.make == criteria.make
            
            # Check if car matches the model criteria, if specified
            matches_model = not criteria.model or car.model == criteria.model
            
            # Check if car belongs to the specified rental group, if specified
            matches_rental_group = not criteria.rental_group or car.rental_group == criteria.rental_group
            
            # Check if car is available for the specified period, if specified
            is_available = not criteria.period or self._is_available(car, criteria.period)

            # The car matches criteria if all individual checks pass
            return all([matches_make, matches_model, matches_rental_group, is_available])
        
        except AttributeError as e:
            raise ValueError(f"Invalid criteria provided: {e}")
        
    def _is_available(self, car, period: DatePeriod):
        if not isinstance(period, DatePeriod):
            raise TypeError("Period must be an instance of DatePeriod.")
        for booking in self.bookings:
            if booking.car == car and DatePeriodUtil.are_overlapping(period, booking.period):
                return False
        return True
    
    def rent_car(self, renter, car, period: DatePeriod, use_blended_price=False):
        with self.lock:  # Ensure thread safety
            # Determine the cost per day
            if use_blended_price:
                cost_per_day = self.get_blended_price(car.rental_group)
                if cost_per_day is None:  # Handle case where blended price cannot be calculated
                    print("Unable to calculate blended price. Using car's default cost per day.")
                    cost_per_day = car.cost_per_day
            else:
                cost_per_day = car.cost_per_day

            try:
                return self._rent_car(renter, car, period, cost_per_day)
            except Exception as e:
                print(f"Failed to rent car: {e}")
                return False
            
    def _rent_car(self, renter, car, period: DatePeriod, cost_per_day):
        # Check if the car is available for the specified period
        if not self._is_available(car, period):
            return False

        # Create a booking with the given DatePeriod object and cost per day
        booking = Booking(renter, car, period, cost_per_day=cost_per_day)
        self.bookings.append(booking)
        return True
        
    def get_new_rentals_for_next_week(self) -> List[Dict[str, str]]:
        # Calculate the start and end date of the next week
        next_week_start = datetime.now().date()
        next_week_end = next_week_start + timedelta(days=7)  # End of next week

        # Get new rentals for the next week
        new_rentals = []
        for booking in self.bookings:
            # Correctly call get_start() as a method
            if booking.period.get_start() >= next_week_start and booking.period.get_start() <= next_week_end:
                rental_info = {
                    'date_rented': booking.period.get_start(),  # Call get_start() here
                    'make': booking.car.make,
                    'model': booking.car.model,
                    'registration_number': booking.car.registration_number
                }
                new_rentals.append(rental_info)
                
        # Sort the new rentals by date
        new_rentals.sort(key=lambda x: x['date_rented'])

        return new_rentals

    def service_car(self, car, period: DatePeriod):
        with self.lock:  # Ensure thread safety
            today = datetime.now().date()

            # Check if the car is booked and the service start date is before today
            if not self._is_available(car, period) and period.get_start() > today:
                return False

            # Proceed with servicing the original car
            booking = Booking(None, car, period, "service")
            self.bookings.append(booking)
            return True

    def transfer_booking(self, renter, original_car, alternative_car, period: DatePeriod):
        with self.lock:  # Ensure thread safety
            for booking in self.bookings:
                # Check if the booking matches the renter and the original car's rental group
                if booking.renter == renter and booking.car == original_car and booking.period == period:
                    # Create a new booking for the alternative car with the same details (including the cost_per_day) 
                    transfer_successful = self._rent_car(renter, alternative_car, period, booking.cost_per_day)
                    if transfer_successful:
                        # Remove the old booking if the transfer was successful
                        self.bookings.remove(booking)
                        return True
                    else:
                        
                        # Transfer was not successful (e.g., alternative car is not available)
                        return False

            # No matching booking found or other failure
            return False

    def get_blended_price(self, group):
        # Filter cars by the specified group
        group_cars = [car for car in self.cars if car.rental_group == group]
        
        # Calculate the average (blended) price of the cars in the group
        if group_cars:
            total_price = sum(car.cost_per_day for car in group_cars)
            blended_price = total_price / len(group_cars)
            return blended_price
        else:
            return None 

    def get_all_bookings(self):
        with self.lock:  # Ensure thread-safe access to the bookings list
            # Return a copy of the bookings list to prevent modification outside the class
            return self.bookings.copy()

    def return_car(self, renter, car):
        # Implementation depends on how you want to handle car returns
        pass