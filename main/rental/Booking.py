from datetime import date
from main.utils.DatePeriod import DatePeriod

class Booking:
    def __init__(self, renter, car, period, booking_type="rental", cost_per_day=None):
        self.renter = renter
        self.car = car
        # Ensure that period is an instance of DatePeriod
        assert isinstance(period, DatePeriod), "period must be an instance of DatePeriod"
        self.period = period
        self.booking_type = booking_type
        # Use provided cost_per_day if given, otherwise default to car's cost_per_day
        self.cost_per_day = cost_per_day if cost_per_day is not None else car.cost_per_day

    def __str__(self):
        if self.renter is not None:
            renter_info = f"{self.renter.last_name}, {self.renter.first_name}"
        else:
            renter_info = "Service - No renter information"

        # Include cost_per_day in the string representation
        return (f"\nBooking: {renter_info}, "
                f"Car: {self.car.registration_number}, "
                f"Period: {self.period.get_start()} to {self.period.get_end()}, "
                f"Type: {self.booking_type}, "
                f"Cost per Day: ${self.cost_per_day}")
